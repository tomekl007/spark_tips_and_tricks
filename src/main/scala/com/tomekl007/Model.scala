package com.tomekl007

import java.util.UUID

case class UserData(userId: String, data: String)

case class UserTransaction(userId: String, amount: Int)

case class InputRecord(uuid: String = UUID.randomUUID().toString, userId: String)
